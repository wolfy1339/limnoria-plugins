BMNBot Plugins
==============
<h4>TravisCI Automated Builds</h4>

[![Build Status](https://travis-ci.org/Brilliant-Minds/Limnoria-Plugins.png?branch=master)](https://travis-ci.org/Brilliant-Minds/Limnoria-Plugins)
<hr/>
The plugins repository for BMNBot. 
Every member of the group is free to join our GitHub organization at https://github.com/Brilliant-Minds and help out with code, artwork, suggestions and generally everything helpful.
Please remember that BMNBot is a Limnoria bot.
